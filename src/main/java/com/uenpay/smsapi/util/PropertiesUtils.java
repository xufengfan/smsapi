package com.uenpay.smsapi.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertiesUtils
{
  public static final String FILE_SEPARATOR = "file.separator";
  public static final String USER_HOME = "user.home";
  private static Logger log = LoggerFactory.getLogger(PropertiesUtils.class);

  private static Properties properties = new Properties();

  public static String getPropertiesVal(String key)
  {
    return properties.getProperty(key);
  }

  static
  {
    InputStream fis = null;
    String configPath = null;
    try {
      String fileSeparator = System.getProperty("file.separator");
      String userHomedir = System.getProperty("user.home");
      configPath = userHomedir + fileSeparator + "configure.properties";
      log.info("configure.properties�����ļ���·��:{}", configPath);
      fis = new FileInputStream(configPath);
      properties.load(fis);
    }
    catch (Exception e) {
    }
    finally {
      if (null != fis)
        try {
          fis.close();
        }
        catch (IOException e)
        {
        }
    }
  }
}