package com.uenpay.smsapi.util;

import java.security.MessageDigest;

public class MD5
{
  public static String getMD5LowerCase32(String message)
  {
    MessageDigest messageDigest = null;
    StringBuffer buf = new StringBuffer("");
    try
    {
      messageDigest = MessageDigest.getInstance("MD5");
      messageDigest.update(message.getBytes("UTF-8"));
      byte[] b = messageDigest.digest();
      for (int offset = 0; offset < b.length; ++offset) {
        int i = b[offset];
        if (i < 0) {
          i += 256;
        }
        if (i < 16) {
          buf.append("0");
        }
        buf.append(Integer.toHexString(i));
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    return buf.toString();
  }
}