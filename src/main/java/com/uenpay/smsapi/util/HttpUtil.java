package com.uenpay.smsapi.util;

import com.alibaba.fastjson.JSON;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang.ObjectUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpUtil
{
  private static final Logger logger = LoggerFactory.getLogger(HttpUtil.class);

  private static String defaultEncoding = "UTF-8";

  private static HttpClient httpClient = null;

  public static String getDefaultEncoding()
  {
    return defaultEncoding;
  }

  public static byte[] postRequest(URI uri, Map<String, Object> params)
    throws ClientProtocolException, IOException
  {
    HttpPost postMethod = new HttpPost(uri);
    List paramsList = new ArrayList();
    byte[] data = null;
    if (params != null) {
      for (String key : params.keySet()) {
        Object value = params.get(key);
        Iterator i$;
        if (value instanceof List) {
          List valueList = (List)value;
          for (i$ = valueList.iterator(); i$.hasNext(); ) { Object val = i$.next();
            paramsList.add(new BasicNameValuePair(key, ObjectUtils.toString(val)));
          }
        }
        else {
          paramsList.add(new BasicNameValuePair(key, ObjectUtils.toString(params.get(key))));
        }
      }
    }
    try
    {
      UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity(paramsList, defaultEncoding);
      postMethod.setEntity(requestEntity);
      HttpResponse response = httpClient.execute(postMethod);
      data = EntityUtils.toByteArray(response.getEntity());
    } finally {
      postMethod.releaseConnection();
    }

    return data;
  }

  public static byte[] postRequestStr(URI uri, Map<String, String> params)
    throws ClientProtocolException, IOException
  {
    HttpPost postMethod = new HttpPost(uri);
    List paramsList = new ArrayList();
    byte[] data = null;
    if (params != null) {
      for (String key : params.keySet()) {
        paramsList.add(new BasicNameValuePair(key, (String)params.get(key)));
      }
    }
    try
    {
      UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity(paramsList, defaultEncoding);
      postMethod.setEntity(requestEntity);
      HttpResponse response = httpClient.execute(postMethod);
      data = EntityUtils.toByteArray(response.getEntity());
    } finally {
      postMethod.releaseConnection();
    }

    return data;
  }

  public static byte[] posJsonRequest(URI uri, Object normalObject)
    throws ClientProtocolException, IOException
  {
    HttpPost postMethod = new HttpPost(uri);
    byte[] data = null;
    try {
      String jsonStr = JSON.toJSONString(normalObject);
      StringEntity stringEntity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);
      postMethod.setEntity(stringEntity);
      HttpResponse response = httpClient.execute(postMethod);
      data = EntityUtils.toByteArray(response.getEntity());
    } finally {
      postMethod.releaseConnection();
    }

    return data;
  }

  private static byte[] posJsonRequest(URI uri, String jsonStr) throws ClientProtocolException, IOException {
    HttpPost postMethod = new HttpPost(uri);
    byte[] data = null;
    try {
      StringEntity stringEntity = new StringEntity(jsonStr, ContentType.APPLICATION_JSON);
      postMethod.setEntity(stringEntity);
      HttpResponse response = httpClient.execute(postMethod);
      data = EntityUtils.toByteArray(response.getEntity());
    } finally {
      postMethod.releaseConnection();
    }

    return data;
  }

  private static byte[] posTextXmlRequest(URI uri, String jsonStr) throws ClientProtocolException, IOException {
    HttpPost postMethod = new HttpPost(uri);
    byte[] data = null;
    try {
      StringEntity stringEntity = new StringEntity(jsonStr, defaultEncoding);
      postMethod.setEntity(stringEntity);
      HttpResponse response = httpClient.execute(postMethod);
      data = EntityUtils.toByteArray(response.getEntity());
    } finally {
      postMethod.releaseConnection();
    }

    return data;
  }

  private static byte[] getRequest(String uri) throws ClientProtocolException, IOException {
    HttpGet postMethod = new HttpGet(uri);
    byte[] data = null;
    try {
      HttpResponse response = httpClient.execute(postMethod);
      data = EntityUtils.toByteArray(response.getEntity());
    } finally {
      postMethod.releaseConnection();
    }

    return data;
  }

  public static String getJsonRequest(String uriStr) throws ClientProtocolException, IOException, URISyntaxException {
    byte[] data = getRequest(uriStr);
    return new String(data, defaultEncoding);
  }

  public static String postJsonRequest(String uriStr, String jsonStr)
    throws ClientProtocolException, IOException, URISyntaxException
  {
    byte[] data = posJsonRequest(new URI(uriStr), jsonStr);
    return new String(data, defaultEncoding);
  }

  public static String postTextXmlRequest(String uriStr, String textXmlStr)
    throws ClientProtocolException, IOException, URISyntaxException
  {
    byte[] data = posTextXmlRequest(new URI(uriStr), textXmlStr);
    return new String(data, defaultEncoding);
  }

  public static String postRequestByString(String uriStr, Map<String, Object> params)
    throws ClientProtocolException, IOException, URISyntaxException
  {
    byte[] data = postRequest(new URI(uriStr), params);
    return new String(data, defaultEncoding);
  }

  public static String postRequestByStringStr(String uriStr, Map<String, String> params)
    throws ClientProtocolException, IOException, URISyntaxException
  {
    byte[] data = postRequestStr(new URI(uriStr), params);
    return new String(data, defaultEncoding);
  }

  public static String postStringRequest(String urlStr, Map<String, String> params, String encoding)
    throws ClientProtocolException, IOException, URISyntaxException
  {
    byte[] data = postStringRequest(new URI(urlStr), params, encoding);
    return new String(data, encoding);
  }

  public static byte[] postStringRequest(URI uri, Map<String, String> params, String encoding)
    throws ClientProtocolException, IOException
  {
    HttpPost postMethod = new HttpPost(uri);
    List paramsList = new ArrayList();
    byte[] data = null;
    if (params != null) {
      for (String key : params.keySet()) {
        paramsList.add(new BasicNameValuePair(key, (String)params.get(key)));
      }
    }
    try
    {
      UrlEncodedFormEntity requestEntity = new UrlEncodedFormEntity(paramsList, encoding);
      postMethod.setEntity(requestEntity);
      HttpResponse response = httpClient.execute(postMethod);
      data = EntityUtils.toByteArray(response.getEntity());
    } finally {
      postMethod.releaseConnection();
    }
    return data;
  }

  public static String postRequestByString(String uriStr, Map<String, Object> params, boolean flag)
    throws ClientProtocolException, IOException, URISyntaxException
  {
    if (flag) {
      Iterator it = params.entrySet().iterator();
      while (it.hasNext())
      {
        if (((Map.Entry)it.next()).getValue() == null);
        it.remove();
      }
    }

    return postRequestByString(uriStr, params);
  }

  public static String sendGBKStr(String callURL, String postData)
  {
    return sendPostRequest(callURL, postData, "GBK");
  }

  public static String sendPostRequest(String callURL, String postData, String encoding)
  {
    HttpURLConnection connection = null;
    DataOutputStream out = null;
    InputStream in = null;
    try {
      URL url = new URL(callURL);

      connection = (HttpURLConnection)url.openConnection();
      connection.setRequestMethod("POST");
      connection.setDoOutput(true);
      connection.setDoInput(true);
      connection.connect();
      out = new DataOutputStream(connection.getOutputStream());
      out.write(postData.getBytes(encoding));
      out.flush();

      int rc = connection.getResponseCode();
      if (rc == 200) {
        String temp = null;
        in = connection.getInputStream();
        BufferedReader data = new BufferedReader(new InputStreamReader(in, encoding));
        StringBuffer result = new StringBuffer();
        while ((temp = data.readLine()) != null) {
          result.append(temp);
          temp = null;
        }
        String str1 = result.toString();
        return str1;
      }
    }
    catch (Exception e)
    {
      logger.error("HttpURLConnection request error", e);
    } finally {
      if (out != null) {
        try {
          out.close();
        } catch (IOException e) {
          logger.error("", e);
        }
      }
      if (in != null) {
        try {
          in.close();
        } catch (IOException e) {
          logger.error("", e);
        }
      }
    }

    return null;
  }

  static
  {
    HttpParams params = new BasicHttpParams();
    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
    HttpProtocolParams.setUserAgent(params, "HttpComponents/1.1");
    HttpProtocolParams.setUseExpectContinue(params, true);

    int REQUEST_TIMEOUT = 120000;
    int SO_TIMEOUT = 120000;

    params.setParameter("http.connection.timeout", Integer.valueOf(REQUEST_TIMEOUT));
    params.setParameter("http.socket.timeout", Integer.valueOf(SO_TIMEOUT));

    SchemeRegistry schreg = new SchemeRegistry();
    schreg.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
    schreg.register(new Scheme("https", 443, SSLSocketFactory.getSocketFactory()));

    PoolingClientConnectionManager pccm = new PoolingClientConnectionManager(schreg);
    pccm.setDefaultMaxPerRoute(50);
    pccm.setMaxTotal(200);

    httpClient = new DefaultHttpClient(pccm, params);
  }
}