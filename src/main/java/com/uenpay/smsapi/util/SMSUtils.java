package com.uenpay.smsapi.util;

import java.io.Serializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.uenpay.smsapi.service.IBaseSendMsgService;
import com.uenpay.smsapi.service.IDhSmsService;

public class SMSUtils
  implements Serializable
{
  private static final long serialVersionUID = 1962982135700816092L;
  static Logger logger = LogManager.getLogger(SMSUtils.class);
  private IBaseSendMsgService jianZhouMsgService;
  private IBaseSendMsgService diyiSmsService;
  private IBaseSendMsgService createBlueSmsService;
  private IDhSmsService dhSmsService;
  private IBaseSendMsgService newCreateBlueSmsService;
  private SmsRunnableHandler smsRunnableHandler;

  public void setJianZhouMsgService(IBaseSendMsgService jianZhouMsgService)
  {
    this.jianZhouMsgService = jianZhouMsgService;
  }

  public void setDiyiSmsService(IBaseSendMsgService diyiSmsService) {
    this.diyiSmsService = diyiSmsService;
  }

  public void setCreateBlueSmsService(IBaseSendMsgService createBlueSmsService) {
    this.createBlueSmsService = createBlueSmsService;
  }

  public void setDhSmsService(IDhSmsService dhSmsService) {
    this.dhSmsService = dhSmsService;
  }

  public void setNewCreateBlueSmsService(IBaseSendMsgService newCreateBlueSmsService) {
    this.newCreateBlueSmsService = newCreateBlueSmsService;
  }

  public void setSmsRunnableHandler(SmsRunnableHandler smsRunnableHandler) {
    this.smsRunnableHandler = smsRunnableHandler;
  }

  public void sendBatchMessage(String smsFlag, String phoneNo, String msg)
    throws Exception
  {
    logger.info("smsFlag:{}�ֻ��ţ�{}--->{}", smsFlag, phoneNo, msg);
    String str = smsFlag; int i = -1; switch (str.hashCode())
    {
    case 48:
      if (str.equals("0")) i = 0; break;
    case 49:
      if (str.equals("1")) i = 1; break;
    case 50:
      if (str.equals("2")) i = 2; break;
    case 51:
      if (str.equals("3")) i = 3;  }
    switch (i)
    {
    case 0:
      sendSmsMessageWithJianZhou(phoneNo, msg);
      break;
    case 1:
      sendSmsMessageWithDiYi(phoneNo, msg);
      break;
    case 2:
      sendSmsMessageWithCreateBlue(phoneNo, msg);
      break;
    case 3:
      sendSmsMessageWithNewCreateBlue(phoneNo, msg);
      break;
    default:
      sendSmsMessageWithDh(phoneNo, msg);
    }
  }

  public void sendSmsMessageWithDiYi(String mobileNo, String content)
    throws Exception
  {
    this.smsRunnableHandler.sendSms(this.diyiSmsService, mobileNo, content);
  }

  public void sendSmsMessageWithJianZhou(String mobileNo, String content)
  {
    this.smsRunnableHandler.sendSms(this.jianZhouMsgService, mobileNo, content);
  }

  public void sendSmsMessageWithCreateBlue(String mobileNo, String content)
    throws Exception
  {
    this.smsRunnableHandler.sendSms(this.createBlueSmsService, mobileNo, content);
  }

  public void sendSmsMessageWithDh(String mobileNo, String content)
  {
    this.smsRunnableHandler.sendSms(this.dhSmsService, null, mobileNo, content);
  }

  public void sendSmsMessageWithDh(String sign, String mobileNo, String content)
  {
    this.smsRunnableHandler.sendSms(this.dhSmsService, sign, mobileNo, content);
  }

  public void sendSmsMessageWithNewCreateBlue(String mobileNo, String content)
  {
    this.smsRunnableHandler.sendSms(this.newCreateBlueSmsService, mobileNo, content);
  }
}