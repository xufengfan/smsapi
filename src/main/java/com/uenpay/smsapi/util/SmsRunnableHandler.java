package com.uenpay.smsapi.util;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uenpay.smsapi.bean.DhSmsResponse;
import com.uenpay.smsapi.bean.NormalSmsSendResponse;
import com.uenpay.smsapi.service.IBaseSendMsgService;
import com.uenpay.smsapi.service.IDhSmsService;
import com.uenpay.smsapi.service.ISysSMSParamService;

public class SmsRunnableHandler
{
  private Logger logger;
  private final ExecutorService executorService;
  private ISysSMSParamService sysSMSParamService;

  public SmsRunnableHandler()
  {
    this.logger = LoggerFactory.getLogger(super.getClass());

    this.executorService = Executors.newCachedThreadPool();
  }

  public ISysSMSParamService getSysSMSParamService()
  {
    return this.sysSMSParamService;
  }

  public void setSysSMSParamService(ISysSMSParamService sysSMSParamService) {
    this.sysSMSParamService = sysSMSParamService;
  }

  public void sendSms(IBaseSendMsgService baseSendMsgService, String mobileNo, String content) {
    this.executorService.execute(new BaseSendMsgRunner(mobileNo, content, baseSendMsgService));
  }

  public void sendSms(IDhSmsService dhSmsService, String sign, String mobileNo, String content) {
    this.executorService.execute(new DhSendMsgRunner(sign, mobileNo, content, dhSmsService));
  }

  private class DhSendMsgRunner
    implements Runnable
  {
    private String sign;
    private String mobileNo;
    private String content;
    private IDhSmsService dhSmsService;

    public DhSendMsgRunner(String paramString1, String paramString2, String paramString3, IDhSmsService paramIDhSmsService)
    {
      this.sign = paramString1;
      this.mobileNo = paramString2;
      this.content = content;
      this.dhSmsService = paramIDhSmsService;
    }

    public void run()
    {
      String prefix = Thread.currentThread().getName() + "--> " + this.dhSmsService.getClass().getSimpleName() + "-->";
      try {
        DhSmsResponse response = this.dhSmsService.sendMessage(this.sign, this.mobileNo, this.content);
        if ((response == null) || (!("0".equals(response.getResult()))))
          SmsRunnableHandler.this.logger.error(prefix + "短信发送失败,手机号:" + this.mobileNo + ",响应信息：" + response);
        else
          SmsRunnableHandler.this.logger.info("{}短信发送完成,手机号:{},响应信息：{}", new Object[] { prefix, this.mobileNo, response });
      }
      catch (ConnectException refused)
      {
        SmsRunnableHandler.this.logger.error(prefix + "请求连接拒绝", refused);
      } catch (SocketTimeoutException timeout) {
        SmsRunnableHandler.this.logger.error(prefix + "请求超时", timeout);
      }
      catch (Exception e) {
        SmsRunnableHandler.this.logger.error(prefix + "未知错误", e);
      }
    }
  }

  private class BaseSendMsgRunner
    implements Runnable
  {
    private String mobileNo;
    private String content;
    private IBaseSendMsgService baseSendMsgService;

    public BaseSendMsgRunner(String paramString1, String paramString2, IBaseSendMsgService paramIBaseSendMsgService)
    {
      this.mobileNo = paramString1;
      this.content = paramString2;
      this.baseSendMsgService = paramIBaseSendMsgService;
    }

    public void run()
    {
      String prefix = Thread.currentThread().getName() + "--> " + this.baseSendMsgService.getClass().getSimpleName() + "-->";
      try {
        NormalSmsSendResponse response = this.baseSendMsgService.sendMsg(this.mobileNo, this.content);
        if ((response == null) || (!("0".equals(response.getCode()))))
          SmsRunnableHandler.this.logger.error(prefix + "短信发送失败,手机号:" + this.mobileNo + ",响应信息：" + response.toString());
        else
          SmsRunnableHandler.this.logger.info("{}短信发送完成,手机号:{},响应信息：{}", new Object[] { prefix, this.mobileNo, response });
      }
      catch (ConnectException refused)
      {
        SmsRunnableHandler.this.logger.error(prefix + "请求连接拒绝,手机号:" + this.mobileNo + ",响应信息：", refused);
      } catch (SocketTimeoutException timeout) {
        SmsRunnableHandler.this.logger.error(prefix + "请求超时", timeout);
      } catch (Exception e) {
        SmsRunnableHandler.this.logger.error(prefix + "未知错误", e);
      }
    }
  }
}