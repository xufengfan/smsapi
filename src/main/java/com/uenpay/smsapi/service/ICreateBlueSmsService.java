package com.uenpay.smsapi.service;

import com.uenpay.smsapi.bean.CreateBlueSmsRequest;
import com.uenpay.smsapi.bean.CreateBlueSmsResponse;

public abstract interface ICreateBlueSmsService
{
  public abstract CreateBlueSmsResponse sendMessage(CreateBlueSmsRequest paramCreateBlueSmsRequest)
    throws Exception;

  public abstract CreateBlueSmsResponse sendMessage(String paramString1, String paramString2)
    throws Exception;
}