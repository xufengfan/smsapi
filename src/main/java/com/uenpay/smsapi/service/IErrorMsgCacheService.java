package com.uenpay.smsapi.service;

public abstract interface IErrorMsgCacheService
{
  public abstract String getErrorMsgByCode(String paramString);
}