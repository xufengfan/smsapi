package com.uenpay.smsapi.service;

public abstract interface IJianZhouMsgService
{
  public abstract int sendMessage(String paramString1, String paramString2)
    throws Exception;
}