package com.uenpay.smsapi.service;

public abstract interface ISysSMSParamService
{
  public abstract String getSysParamValue(String paramString);
}