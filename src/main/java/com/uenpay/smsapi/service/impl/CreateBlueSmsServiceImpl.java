package com.uenpay.smsapi.service.impl;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uenpay.smsapi.bean.CreateBlueSmsRequest;
import com.uenpay.smsapi.bean.CreateBlueSmsResponse;
import com.uenpay.smsapi.bean.NormalSmsSendResponse;
import com.uenpay.smsapi.service.IBaseSendMsgService;
import com.uenpay.smsapi.service.ICreateBlueSmsService;
import com.uenpay.smsapi.util.HttpUtil;
import com.uenpay.smsapi.util.PropertiesUtils;

public class CreateBlueSmsServiceImpl
  implements ICreateBlueSmsService, IBaseSendMsgService
{
  private Logger logger = LoggerFactory.getLogger(super.getClass());
  private String url;
  private String account;
  private String password;
  private final String dyPrefix = "cl_";

  private String getBeanValue(String name) {
    return PropertiesUtils.getPropertiesVal("cl_" + name);
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  public String getAccount()
  {
    return this.account;
  }

  public void setAccount(String account)
  {
    this.account = account;
  }

  public String getPassword()
  {
    return this.password;
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  public CreateBlueSmsServiceImpl()
  {
    setUrl(getBeanValue("url"));
    setAccount(getBeanValue("account"));
    setPassword(getBeanValue("password"));
  }

  public CreateBlueSmsResponse sendMessage(CreateBlueSmsRequest request)
    throws Exception
  {
    this.logger.info("{}发送创蓝短信内容：{}", request.getMobile(), request.getMsg());
    Map params = assembleParam(request.getMobile(), request.getMsg());
    String fixedFormatText = HttpUtil.postRequestByString(this.url, params);
    this.logger.info("{}发送短信创蓝响应短信内容：{}", request.getMobile(), fixedFormatText);
    CreateBlueSmsResponse response = new CreateBlueSmsResponse(fixedFormatText);
    return response;
  }

  private Map<String, Object> assembleParam(String mobile, String msg)
  {
    Map maps = new HashMap();
    maps.put("account", this.account);
    maps.put("pswd", this.password);
    maps.put("mobile", mobile);
    maps.put("msg", msg);
    maps.put("needstatus", "false");
    return maps;
  }

  public CreateBlueSmsResponse sendMessage(String mobile, String msg)
    throws Exception
  {
    CreateBlueSmsRequest request = new CreateBlueSmsRequest(mobile, msg);
    return sendMessage(request);
  }

  public NormalSmsSendResponse sendMsg(String mobileNo, String content)
    throws Exception
  {
    CreateBlueSmsResponse response = sendMessage(mobileNo, content);
    NormalSmsSendResponse sendResponse = new NormalSmsSendResponse();
    sendResponse.setCode(response.getRespstatus());
    sendResponse.setMsg("发送请求完成");
    sendResponse.setOriginResponseMsg(response.getOriginalStr());
    return sendResponse;
  }
}