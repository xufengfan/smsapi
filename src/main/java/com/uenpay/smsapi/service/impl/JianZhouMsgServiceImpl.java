package com.uenpay.smsapi.service.impl;

import com.jianzhou.sdk.BusinessService;
import com.uenpay.smsapi.bean.NormalSmsSendResponse;
import com.uenpay.smsapi.service.IBaseSendMsgService;
import com.uenpay.smsapi.service.IJianZhouMsgService;
import com.uenpay.smsapi.util.PropertiesUtils;

public class JianZhouMsgServiceImpl
  implements IJianZhouMsgService, IBaseSendMsgService
{
  private final BusinessService businessService = new BusinessService();

  private final String dyPrefix = "jz_";
  private String url;
  private String account;
  private String password;
  private String tailMsg;

  private String getBeanValue(String name)
  {
    return PropertiesUtils.getPropertiesVal("jz_" + name);
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getAccount() {
    return this.account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getTailMsg() {
    return this.tailMsg;
  }

  public void setTailMsg(String tailMsg) {
    this.tailMsg = tailMsg;
  }

  public String getDyPrefix() {
    return "jz_";
  }

  public BusinessService getBusinessService() {
    return this.businessService;
  }

  public JianZhouMsgServiceImpl()
  {
    setUrl(getBeanValue("url"));
    setAccount(getBeanValue("account"));
    setPassword(getBeanValue("password"));
    setTailMsg(getBeanValue("tailMsg"));
  }

  public int sendMessage(String mobileNo, String content)
    throws Exception
  {
    this.businessService.setWebService(getUrl());
    int result = this.businessService.sendBatchMessage(getAccount(), getPassword(), mobileNo, content + getTailMsg());
    return result;
  }

  public NormalSmsSendResponse sendMsg(String mobileNo, String content)
    throws Exception
  {
    int result = sendMessage(mobileNo, content);
    NormalSmsSendResponse sendResponse = new NormalSmsSendResponse();
    sendResponse.setCode("0");
    sendResponse.setMsg("�����������");
    sendResponse.setOriginResponseMsg(String.valueOf(result));
    return sendResponse;
  }
}