package com.uenpay.smsapi.service.impl;

import com.alibaba.fastjson.JSON;
import com.uenpay.smsapi.bean.NormalSmsSendResponse;
import com.uenpay.smsapi.bean.SmsSendRequest;
import com.uenpay.smsapi.bean.SmsSendResponse;
import com.uenpay.smsapi.service.IBaseSendMsgService;
import com.uenpay.smsapi.service.INewCreateBlueSmsService;
import com.uenpay.smsapi.util.HttpUtil;
import com.uenpay.smsapi.util.PropertiesUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewCreateBlueSmsServiceImpl
  implements INewCreateBlueSmsService, IBaseSendMsgService
{
  private Logger logger = LoggerFactory.getLogger(super.getClass());
  private static String url;
  private String account;
  private String password;
  private String sendtime;
  private String report;
  private String extend;
  private final String dyPrefix = "new_cl_";

  private String getBeanValue(String name) {
    return PropertiesUtils.getPropertiesVal("new_cl_" + name);
  }

  public NewCreateBlueSmsServiceImpl()
  {
    setUrl(getBeanValue("url"));
    setAccount(getBeanValue("account"));
    setPassword(getBeanValue("password"));
    setSendtime(getBeanValue("sendtime"));
    setReport(getBeanValue("report"));
    setExtend(getBeanValue("extend"));
  }

  public static String getUrl() {
    return url;
  }

  public static void setUrl(String url)
  {
    url = url;
  }

  public String getAccount() {
    return this.account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getSendtime() {
    return this.sendtime;
  }

  public void setSendtime(String sendtime) {
    this.sendtime = sendtime;
  }

  public String getReport() {
    return this.report;
  }

  public void setReport(String report) {
    this.report = report;
  }

  public String getExtend() {
    return this.extend;
  }

  public void setExtend(String extend) {
    this.extend = extend;
  }

  public String getDyPrefix() {
    return "new_cl_";
  }

  public SmsSendResponse sendMessage(String mobile, String msg) throws Exception
  {
    SmsSendRequest request = new SmsSendRequest(mobile, msg);
    return sendMessage(request);
  }

  public SmsSendResponse sendMessage(SmsSendRequest request)
    throws Exception
  {
    request.setAccount(this.account);
    request.setPassword(this.password);
    String jsonStr = JSON.toJSONString(request);
    this.logger.info("【短信下发】请求信息  requestJson:" + jsonStr);
    String resStr = HttpUtil.postJsonRequest(url + "send/json", jsonStr);
    this.logger.info("【短信下发】返回信息 response:" + resStr);
    SmsSendResponse response = (SmsSendResponse)JSON.parseObject(resStr, SmsSendResponse.class);
    return response;
  }

  public NormalSmsSendResponse sendMsg(String mobileNo, String content)
    throws Exception
  {
    SmsSendResponse response = sendMessage(mobileNo, content);
    NormalSmsSendResponse sendResponse = new NormalSmsSendResponse();
    sendResponse.setCode(response.getCode());
    sendResponse.setMsg("发送请求完成");
    sendResponse.setOriginResponseMsg(response.getErrorMsg());
    return sendResponse;
  }
}