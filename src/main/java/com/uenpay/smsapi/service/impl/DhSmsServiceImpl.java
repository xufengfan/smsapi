package com.uenpay.smsapi.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.uenpay.smsapi.bean.DhSmsBackResponse;
import com.uenpay.smsapi.bean.DhSmsBalanceResponse;
import com.uenpay.smsapi.bean.DhSmsBaseRequest;
import com.uenpay.smsapi.bean.DhSmsDeliverResponse;
import com.uenpay.smsapi.bean.DhSmsRequest;
import com.uenpay.smsapi.bean.DhSmsResponse;
import com.uenpay.smsapi.bean.DhbatchSmsRequest;
import com.uenpay.smsapi.bean.DhbatchSmsResponse;
import com.uenpay.smsapi.bean.NormalSmsSendResponse;
import com.uenpay.smsapi.service.IBaseSendMsgService;
import com.uenpay.smsapi.service.IDhSmsService;
import com.uenpay.smsapi.util.HttpUtil;
import com.uenpay.smsapi.util.MD5;
import com.uenpay.smsapi.util.PropertiesUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.UUID;
import org.apache.http.client.ClientProtocolException;

public class DhSmsServiceImpl
  implements IDhSmsService, IBaseSendMsgService
{
  private String url;
  private String account;
  private String password;
  private final String dyPrefix = "dh3t_";

  private String getBeanValue(String name) {
    return PropertiesUtils.getPropertiesVal("dh3t_" + name);
  }

  public DhSmsServiceImpl()
  {
    setUrl(getBeanValue("url"));
    setAccount(getBeanValue("account"));
    setPassword(MD5.getMD5LowerCase32(getBeanValue("password")));
  }

  public String getUrl() {
    return this.url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getAccount() {
    return this.account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public DhSmsResponse sendMessage(DhSmsRequest request) throws ClientProtocolException, IOException, URISyntaxException
  {
    request.setAccount(this.account);
    request.setPassword(this.password);

    String uuidStr = UUID.randomUUID().toString().toUpperCase();
    request.setMsgid(uuidStr);
    String jsonStr = JSON.toJSONString(request);

    DhSmsResponse res = new DhSmsResponse();

    String resStr = HttpUtil.postJsonRequest(this.url + "Submit", jsonStr);
    res = (DhSmsResponse)JSONObject.parseObject(resStr, DhSmsResponse.class);
    return res;
  }

  public DhSmsResponse sendMessage(String phones, String content)
    throws Exception
  {
    DhSmsRequest req = new DhSmsRequest(phones, content);
    return sendMessage(req);
  }

  public DhSmsResponse sendMessage(String sign, String phones, String content)
    throws Exception
  {
    DhSmsRequest req = new DhSmsRequest(phones, content);
    req.setSign(sign);
    return sendMessage(req);
  }

  public NormalSmsSendResponse sendMsg(String mobileNo, String content)
    throws Exception
  {
    DhSmsResponse res = sendMessage(mobileNo, content);
    NormalSmsSendResponse sendResponse = new NormalSmsSendResponse();
    sendResponse.setCode(res.getResult());
    sendResponse.setMsg(res.getDesc());
    sendResponse.setSendid(res.getMsgid());
    sendResponse.setFailPhones(res.getFailPhones());
    return sendResponse;
  }

  public DhSmsBackResponse getMessageReports()
    throws ClientProtocolException, IOException, URISyntaxException
  {
    DhSmsBaseRequest request = new DhSmsBaseRequest();
    request.setAccount(this.account);
    request.setPassword(this.password);
    String jsonStr = JSON.toJSONString(request);

    DhSmsBackResponse res = new DhSmsBackResponse();

    String resStr = HttpUtil.postJsonRequest(this.url + "Report", jsonStr);
    res = (DhSmsBackResponse)JSONObject.parseObject(resStr, DhSmsBackResponse.class);
    return res;
  }

  public DhSmsBalanceResponse getBalance() throws ClientProtocolException, IOException, URISyntaxException
  {
    DhSmsBaseRequest request = new DhSmsBaseRequest();
    request.setAccount(this.account);
    request.setPassword(this.password);
    String jsonStr = JSON.toJSONString(request);

    DhSmsBalanceResponse smsBalance = new DhSmsBalanceResponse();
    String resStr = HttpUtil.postJsonRequest(this.url + "Balance", jsonStr);
    smsBalance = (DhSmsBalanceResponse)JSONObject.parseObject(resStr, DhSmsBalanceResponse.class);
    return smsBalance;
  }

  public DhbatchSmsResponse sendBatchMessage(DhbatchSmsRequest request) throws ClientProtocolException, IOException, URISyntaxException
  {
    request.setAccount(this.account);
    request.setPassword(this.password);
    String jsonStr = JSON.toJSONString(request);
    String resStr = HttpUtil.postJsonRequest(this.url + "BatchSubmit", jsonStr);
    DhbatchSmsResponse res = (DhbatchSmsResponse)JSONObject.parseObject(resStr, DhbatchSmsResponse.class);
    return res;
  }

  public DhSmsDeliverResponse getDeliverMessage()
    throws ClientProtocolException, IOException, URISyntaxException
  {
    DhSmsBaseRequest request = new DhSmsBaseRequest();
    request.setAccount(this.account);
    request.setPassword(this.password);
    String jsonStr = JSON.toJSONString(request);
    String resStr = HttpUtil.postJsonRequest(this.url + "Deliver", jsonStr);
    DhSmsDeliverResponse res = (DhSmsDeliverResponse)JSONObject.parseObject(resStr, DhSmsDeliverResponse.class);
    return res;
  }
}