package com.uenpay.smsapi.service.impl;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uenpay.smsapi.bean.NormalSmsSendRequest;
import com.uenpay.smsapi.bean.NormalSmsSendResponse;
import com.uenpay.smsapi.service.IBaseSendMsgService;
import com.uenpay.smsapi.service.IDiYiSmsService;
import com.uenpay.smsapi.util.HttpUtil;
import com.uenpay.smsapi.util.PropertiesUtils;

public class DiYiSmsServiceImpl
  implements IDiYiSmsService, IBaseSendMsgService
{
  private Logger logger;
  private String url;
  private String account;
  private String password;
  private String tailMsg;
  private final String dyPrefix = "dy_";

  public DiYiSmsServiceImpl()
  {
    this.logger = LoggerFactory.getLogger(super.getClass());

  }

  private String getBeanValue(String name) {
    return PropertiesUtils.getPropertiesVal("dy_" + name);
  }

  private String getUrl() {
    return (((null == this.url) || ("".equals(this.url.trim()))) ? getBeanValue("url") : this.url);
  }

  public void setUrl(String url)
  {
    this.url = url;
  }

  private String getAccount()
  {
    return (((null == this.account) || ("".equals(this.account.trim()))) ? getBeanValue("account") : this.account);
  }

  public void setAccount(String account)
  {
    this.account = account;
  }

  private String getPassword()
  {
    return (((null == this.password) || ("".equals(this.password.trim()))) ? getBeanValue("password") : this.password);
  }

  public void setPassword(String password)
  {
    this.password = password;
  }

  private String getTailMsg()
  {
    return (((null == this.tailMsg) || ("".equals(this.tailMsg.trim()))) ? getBeanValue("tailMsg") : this.tailMsg);
  }

  public void setTailMsg(String tailMsg)
  {
    this.tailMsg = tailMsg;
  }

  public NormalSmsSendResponse sendNormalMessage(NormalSmsSendRequest request)
    throws Exception
  {
    request.setName(getAccount());
    request.setPwd(getPassword());
    if (StringUtils.isEmpty(request.getSign())) {
      request.setSign(getTailMsg());
    }
    this.logger.info("{}发送第翼短信内容：{}", request.getMobile(), request.getContent());
    Map maps = assembleParam(request.getMobile(), request.getContent());
    String str = null;
    str = HttpUtil.postRequestByString(getUrl(), maps);
    this.logger.info("{}发送短信第翼响应短信内容：{}", request.getMobile(), str);
    NormalSmsSendResponse response = new NormalSmsSendResponse(str);
    return response;
  }

  public NormalSmsSendResponse sendNormalMessage(String mobileNo, String content)
    throws Exception
  {
    NormalSmsSendRequest request = new NormalSmsSendRequest();
    request.setMobile(mobileNo);
    request.setContent(content);
    return sendNormalMessage(request);
  }

  public NormalSmsSendResponse sendMsg(String mobileNo, String content)
    throws Exception
  {
    return sendNormalMessage(mobileNo, content);
  }

  private Map<String, Object> assembleParam(String mobile, String msg)
  {
    Map maps = new HashMap();
    maps.put("name", this.account);
    maps.put("pwd", this.password);
    maps.put("mobile", mobile);
    maps.put("content", msg);
    maps.put("sign", this.tailMsg);
    return maps;
  }
}