package com.uenpay.smsapi.service;

import com.uenpay.smsapi.bean.NormalSmsSendRequest;
import com.uenpay.smsapi.bean.NormalSmsSendResponse;

public abstract interface IDiYiSmsService
{
  public abstract NormalSmsSendResponse sendNormalMessage(NormalSmsSendRequest paramNormalSmsSendRequest)
    throws Exception;

  public abstract NormalSmsSendResponse sendNormalMessage(String paramString1, String paramString2)
    throws Exception;
}