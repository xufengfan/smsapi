package com.uenpay.smsapi.service;

import com.uenpay.smsapi.bean.NormalSmsSendResponse;

public abstract interface IBaseSendMsgService
{
  public abstract NormalSmsSendResponse sendMsg(String paramString1, String paramString2)
    throws Exception;
}