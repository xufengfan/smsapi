package com.uenpay.smsapi.service;

import java.io.IOException;
import java.net.URISyntaxException;
import org.apache.http.client.ClientProtocolException;

import com.uenpay.smsapi.bean.DhSmsBackResponse;
import com.uenpay.smsapi.bean.DhSmsBalanceResponse;
import com.uenpay.smsapi.bean.DhSmsDeliverResponse;
import com.uenpay.smsapi.bean.DhSmsRequest;
import com.uenpay.smsapi.bean.DhSmsResponse;
import com.uenpay.smsapi.bean.DhbatchSmsRequest;
import com.uenpay.smsapi.bean.DhbatchSmsResponse;

public abstract interface IDhSmsService
{
  public abstract DhSmsResponse sendMessage(DhSmsRequest paramDhSmsRequest)
    throws Exception;

  public abstract DhSmsResponse sendMessage(String paramString1, String paramString2)
    throws Exception;

  public abstract DhSmsResponse sendMessage(String paramString1, String paramString2, String paramString3)
    throws Exception;

  public abstract DhSmsBackResponse getMessageReports()
    throws ClientProtocolException, IOException, URISyntaxException;

  public abstract DhSmsBalanceResponse getBalance()
    throws ClientProtocolException, IOException, URISyntaxException;

  public abstract DhbatchSmsResponse sendBatchMessage(DhbatchSmsRequest paramDhbatchSmsRequest)
    throws ClientProtocolException, IOException, URISyntaxException;

  public abstract DhSmsDeliverResponse getDeliverMessage()
    throws ClientProtocolException, IOException, URISyntaxException;
}