package com.uenpay.smsapi.service;

import com.uenpay.smsapi.bean.SmsSendRequest;
import com.uenpay.smsapi.bean.SmsSendResponse;

public abstract interface INewCreateBlueSmsService
{
  public abstract SmsSendResponse sendMessage(SmsSendRequest paramSmsSendRequest)
    throws Exception;

  public abstract SmsSendResponse sendMessage(String paramString1, String paramString2)
    throws Exception;
}