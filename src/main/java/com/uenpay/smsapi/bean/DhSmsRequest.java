package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhSmsRequest extends DhSmsBaseRequest
  implements Serializable
{
  private static final long serialVersionUID = 4579878467905701120L;
  private String msgid;
  private String phones;
  private String content;
  private String sign;
  private String sendtime;
  private String subcode;

  public DhSmsRequest()
  {
  }

  public DhSmsRequest(String phones, String content)
  {
    this.content = content;
    this.phones = phones;
  }

  public String getMsgid() {
    return this.msgid;
  }

  public void setMsgid(String msgid) {
    this.msgid = msgid;
  }

  public String getPhones() {
    return this.phones;
  }

  public void setPhones(String phones) {
    this.phones = phones;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getSign() {
    return this.sign;
  }

  public void setSign(String sign) {
    this.sign = sign;
  }

  public String getSendtime() {
    return this.sendtime;
  }

  public void setSendtime(String sendtime) {
    this.sendtime = sendtime;
  }

  public String getSubcode() {
    return this.subcode;
  }

  public void setSubcode(String subcode) {
    this.subcode = subcode;
  }
}