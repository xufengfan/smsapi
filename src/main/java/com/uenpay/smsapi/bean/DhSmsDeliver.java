package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhSmsDeliver
  implements Serializable
{
  private static final long serialVersionUID = -2077522381405126067L;
  private String phone;
  private String content;
  private String subcode;
  private String delivertime;

  public String getPhone()
  {
    return this.phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getContent() {
    return this.content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getSubcode() {
    return this.subcode;
  }

  public void setSubcode(String subcode) {
    this.subcode = subcode;
  }

  public String getDelivertime() {
    return this.delivertime;
  }

  public void setDelivertime(String delivertime) {
    this.delivertime = delivertime;
  }
}