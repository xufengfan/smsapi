package com.uenpay.smsapi.bean;

import java.io.Serializable;
import java.util.List;

public class DhbatchSmsResponse
  implements Serializable
{
  private static final long serialVersionUID = 6616044090800273493L;
  private String result;
  private String desc;
  private List<DhbatchReport> data;

  public String getResult()
  {
    return this.result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public List<DhbatchReport> getData() {
    return this.data;
  }

  public void setData(List<DhbatchReport> data) {
    this.data = data;
  }
}