package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhSmsBalance
  implements Serializable
{
  private static final long serialVersionUID = 112559610035203634L;
  private String amount;
  private String number;
  private String freeze;

  public String getAmount()
  {
    return this.amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getNumber() {
    return this.number;
  }

  public void setNumber(String number) {
    this.number = number;
  }

  public String getFreeze() {
    return this.freeze;
  }

  public void setFreeze(String freeze) {
    this.freeze = freeze;
  }
}