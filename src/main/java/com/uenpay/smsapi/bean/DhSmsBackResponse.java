package com.uenpay.smsapi.bean;

import java.io.Serializable;
import java.util.List;

public class DhSmsBackResponse
  implements Serializable
{
  private static final long serialVersionUID = 3142575832745059016L;
  private String result;
  private String desc;
  private List<DhSmsBackReport> reports;

  public String getResult()
  {
    return this.result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public List<DhSmsBackReport> getReports() {
    return this.reports;
  }

  public void setReports(List<DhSmsBackReport> reports) {
    this.reports = reports;
  }
}