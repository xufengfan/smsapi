package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhSmsResponse
  implements Serializable
{
  private static final long serialVersionUID = -8424688631360540088L;
  private String msgid;
  private String result;
  private String desc;
  private String failPhones;

  public String getMsgid()
  {
    return this.msgid;
  }

  public void setMsgid(String msgid) {
    this.msgid = msgid;
  }

  public String getResult() {
    return this.result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public String getFailPhones() {
    return this.failPhones;
  }

  public void setFailPhones(String failPhones) {
    this.failPhones = failPhones;
  }

  public String toString()
  {
    return "DhSmsResponse [msgid=" + this.msgid + ", result=" + this.result + ", desc=" + this.desc + ", failPhones=" + this.failPhones + "]";
  }
}