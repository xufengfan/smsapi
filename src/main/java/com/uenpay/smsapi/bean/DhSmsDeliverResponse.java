package com.uenpay.smsapi.bean;

import java.io.Serializable;
import java.util.List;

public class DhSmsDeliverResponse
  implements Serializable
{
  private static final long serialVersionUID = -5051786679701985585L;
  private String result;
  private String desc;
  private List<DhSmsDeliver> delivers;

  public String getResult()
  {
    return this.result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public List<DhSmsDeliver> getDelivers() {
    return this.delivers;
  }

  public void setDelivers(List<DhSmsDeliver> delivers) {
    this.delivers = delivers;
  }
}