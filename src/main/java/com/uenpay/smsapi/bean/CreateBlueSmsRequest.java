package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class CreateBlueSmsRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String account;
  private String pswd;
  private String mobile;
  private String msg;
  private Boolean needstatus;
  private String product;
  private String extno;

  public CreateBlueSmsRequest()
  {
  }

  public CreateBlueSmsRequest(String mobile, String msg)
  {
    this.mobile = mobile;
    this.msg = msg;
  }

  public String getAccount() {
    return this.account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getPswd() {
    return this.pswd;
  }

  public void setPswd(String pswd) {
    this.pswd = pswd;
  }

  public String getMobile() {
    return this.mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getMsg() {
    return this.msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public Boolean isNeedstatus() {
    return this.needstatus;
  }

  public void setNeedstatus(Boolean needstatus) {
    this.needstatus = needstatus;
  }

  public String getProduct() {
    return this.product;
  }

  public void setProduct(String product) {
    this.product = product;
  }

  public String getExtno() {
    return this.extno;
  }

  public void setExtno(String extno) {
    this.extno = extno;
  }

  public String toString()
  {
    return "CreateBlueSmsRequest [account=" + this.account + ", pswd=" + this.pswd + ", mobile=" + this.mobile + ", msg=" + this.msg + ", needstatus=" + this.needstatus + ", product=" + this.product + ", extno=" + this.extno + "]";
  }
}