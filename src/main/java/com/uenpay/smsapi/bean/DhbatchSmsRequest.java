package com.uenpay.smsapi.bean;

import java.util.List;

public class DhbatchSmsRequest extends DhSmsBaseRequest
{
  private static final long serialVersionUID = -156976305873982631L;
  private List<DhBatchBaseSmsRequest> data;

  public List<DhBatchBaseSmsRequest> getData()
  {
    return this.data;
  }

  public void setData(List<DhBatchBaseSmsRequest> data) {
    this.data = data;
  }
}