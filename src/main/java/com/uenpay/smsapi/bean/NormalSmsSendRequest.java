package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class NormalSmsSendRequest
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String name;
  private String pwd;
  private String content;
  private String mobile;
  private String stime;
  private String sign;
  private String type = "pt";
  private String extno;

  public NormalSmsSendRequest()
  {
  }

  public NormalSmsSendRequest(String content, String mobile, String stime, String sign, String type, String extno)
  {
    this.content = content;
    this.mobile = mobile;
    this.stime = stime;
    this.sign = sign;
    this.type = type;
    this.extno = extno;
  }

  public String getName()
  {
    return this.name; }

  public void setName(String name) {
    this.name = name; }

  public String getPwd() {
    return this.pwd; }

  public void setPwd(String pwd) {
    this.pwd = pwd; }

  public String getContent() {
    return this.content; }

  public void setContent(String content) {
    this.content = content; }

  public String getMobile() {
    return this.mobile; }

  public void setMobile(String mobile) {
    this.mobile = mobile; }

  public String getStime() {
    return this.stime; }

  public void setStime(String stime) {
    this.stime = stime; }

  public String getSign() {
    return this.sign; }

  public void setSign(String sign) {
    this.sign = sign; }

  public String getType() {
    return this.type; }

  public void setType(String type) {
    this.type = type; }

  public String getExtno() {
    return this.extno; }

  public void setExtno(String extno) {
    this.extno = extno;
  }

  public String toString() {
    return "NormalSmsSendRequest [name=" + this.name + ", pwd=" + this.pwd + ", content=" + this.content + ", mobile=" + this.mobile + ", stime=" + this.stime + ", sign=" + this.sign + ", type=" + this.type + ", extno=" + this.extno + "]";
  }
}