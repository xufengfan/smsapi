package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class SmsSendRequest
  implements Serializable
{
  private static final long serialVersionUID = 4259596224994479233L;
  private String account;
  private String password;
  private String msg;
  private String phone;
  private String sendtime;
  private String report;
  private String extend;
  private String uid;

  public SmsSendRequest()
  {
  }

  public SmsSendRequest(String phone, String msg)
  {
    this.msg = msg;
    this.phone = phone;
  }

  public String getAccount() {
    return this.account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getMsg() {
    return this.msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getPhone() {
    return this.phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getSendtime() {
    return this.sendtime;
  }

  public void setSendtime(String sendtime) {
    this.sendtime = sendtime;
  }

  public String getReport() {
    return this.report;
  }

  public void setReport(String report) {
    this.report = report;
  }

  public String getExtend() {
    return this.extend;
  }

  public void setExtend(String extend) {
    this.extend = extend;
  }

  public String getUid() {
    return this.uid;
  }

  public void setUid(String uid) {
    this.uid = uid;
  }

  public String toString()
  {
    return "SmsSendRequest [account=" + this.account + ", password=" + this.password + ", msg=" + this.msg + ", phone=" + this.phone + ", sendtime=" + this.sendtime + ", report=" + this.report + ", extend=" + this.extend + ", uid=" + this.uid + "]";
  }
}