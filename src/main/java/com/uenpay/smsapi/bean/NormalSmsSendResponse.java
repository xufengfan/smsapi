package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class NormalSmsSendResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String code;
  private String sendid;
  private String invalidcount;
  private String successcount;
  private String blackcount;
  private String msg;
  private String originResponseMsg;
  private String failPhones;

  public String getOriginResponseMsg(int limitSize)
  {
    if ((this.originResponseMsg == null) || (this.originResponseMsg.length() <= limitSize)) {
      return this.originResponseMsg;
    }
    return this.originResponseMsg.substring(0, limitSize);
  }

  public NormalSmsSendResponse()
  {
  }

  public NormalSmsSendResponse(String fixedFormatText)
  {
    this.originResponseMsg = fixedFormatText;
    String[] splitData = fixedFormatText.split(",");
    this.code = fetchField(splitData, 0);

    if ("0".equals(this.code)) {
      this.sendid = fetchField(splitData, 1);
      this.invalidcount = fetchField(splitData, 2);
      this.successcount = fetchField(splitData, 3);
      this.blackcount = fetchField(splitData, 4);
      this.msg = fetchField(splitData, 5);
    } else {
      this.msg = fetchField(splitData, 1);
    }
  }

  public String getCode()
  {
    return this.code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getSendid() {
    return this.sendid;
  }

  public void setSendid(String sendid) {
    this.sendid = sendid;
  }

  public String getInvalidcount() {
    return this.invalidcount;
  }

  public void setInvalidcount(String invalidcount) {
    this.invalidcount = invalidcount;
  }

  public String getSuccesscount() {
    return this.successcount;
  }

  public void setSuccesscount(String successcount) {
    this.successcount = successcount;
  }

  public String getBlackcount() {
    return this.blackcount;
  }

  public void setBlackcount(String blackcount) {
    this.blackcount = blackcount;
  }

  public String getMsg() {
    return this.msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public String getOriginResponseMsg() {
    return this.originResponseMsg;
  }

  public void setOriginResponseMsg(String originResponseMsg) {
    this.originResponseMsg = originResponseMsg;
  }

  public String getFailPhones() {
    return this.failPhones;
  }

  public void setFailPhones(String failPhones) {
    this.failPhones = failPhones;
  }

  private String fetchField(String[] splitData, int index)
  {
    if (splitData.length > index) {
      return splitData[index];
    }
    return null;
  }

  public String toString()
  {
    return "NormalSmsSendResponse [code=" + this.code + ", sendid=" + this.sendid + ", invalidcount=" + this.invalidcount + ", successcount=" + this.successcount + ", blackcount=" + this.blackcount + ", msg=" + this.msg + "]";
  }
}