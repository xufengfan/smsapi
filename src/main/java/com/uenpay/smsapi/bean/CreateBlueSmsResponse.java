package com.uenpay.smsapi.bean;

import java.io.Serializable;
import org.apache.commons.lang.StringUtils;

public class CreateBlueSmsResponse
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private String resptime;
  private String respstatus;
  private String msgid;
  private String originalStr;

  public CreateBlueSmsResponse(String originalStr)
  {
    this.originalStr = originalStr;
    String[] lines = StringUtils.split(originalStr, "\n");
    if (lines.length > 0) {
      String[] firstLine = lines[0].split(",");
      if (firstLine.length > 0) {
        this.resptime = firstLine[0];
      }
      if (firstLine.length > 1) {
        this.respstatus = firstLine[1];
      }
    }

    if (lines.length > 1)
      this.msgid = lines[1];
  }

  public String getResptime()
  {
    return this.resptime;
  }

  public void setResptime(String resptime)
  {
    this.resptime = resptime;
  }

  public String getRespstatus()
  {
    return this.respstatus;
  }

  public void setRespstatus(String respstatus)
  {
    this.respstatus = respstatus;
  }

  public String getMsgid()
  {
    return this.msgid;
  }

  public void setMsgid(String msgid)
  {
    this.msgid = msgid;
  }

  public String getOriginalStr()
  {
    return this.originalStr;
  }

  public String toString()
  {
    return "CreateBlueSmsResponse [resptime=" + this.resptime + ", respstatus=" + this.respstatus + ", msgid=" + this.msgid + "]";
  }
}