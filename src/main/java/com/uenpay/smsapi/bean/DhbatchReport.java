package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhbatchReport
  implements Serializable
{
  private static final long serialVersionUID = 7331383522809162283L;
  private String msgid;
  private String status;
  private String desc;
  private String failPhones;

  public String getMsgid()
  {
    return this.msgid;
  }

  public void setMsgid(String msgid) {
    this.msgid = msgid;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public String getFailPhones() {
    return this.failPhones;
  }

  public void setFailPhones(String failPhones) {
    this.failPhones = failPhones;
  }
}