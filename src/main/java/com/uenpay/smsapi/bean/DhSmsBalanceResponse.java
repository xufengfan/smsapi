package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhSmsBalanceResponse
  implements Serializable
{
  private static final long serialVersionUID = -4014491952687399452L;
  private String result;
  private String desc;
  private DhSmsBalance smsBalance;

  public String getResult()
  {
    return this.result;
  }

  public void setResult(String result) {
    this.result = result;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public DhSmsBalance getSmsBalance() {
    return this.smsBalance;
  }

  public void setSmsBalance(DhSmsBalance smsBalance) {
    this.smsBalance = smsBalance;
  }
}