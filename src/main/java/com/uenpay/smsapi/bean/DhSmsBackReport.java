package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhSmsBackReport
  implements Serializable
{
  private static final long serialVersionUID = 8154524741538148208L;
  private String msgid;
  private String phone;
  private String status;
  private String desc;
  private String wgcode;
  private String time;
  private String smsCount;
  private String smsIndex;

  public String getMsgid()
  {
    return this.msgid;
  }

  public void setMsgid(String msgid) {
    this.msgid = msgid;
  }

  public String getPhone() {
    return this.phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getStatus() {
    return this.status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getDesc() {
    return this.desc;
  }

  public void setDesc(String desc) {
    this.desc = desc;
  }

  public String getWgcode() {
    return this.wgcode;
  }

  public void setWgcode(String wgcode) {
    this.wgcode = wgcode;
  }

  public String getTime() {
    return this.time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getSmsCount() {
    return this.smsCount;
  }

  public void setSmsCount(String smsCount) {
    this.smsCount = smsCount;
  }

  public String getSmsIndex() {
    return this.smsIndex;
  }

  public void setSmsIndex(String smsIndex) {
    this.smsIndex = smsIndex;
  }
}