package com.uenpay.smsapi.bean;

import java.io.Serializable;

public class DhSmsBaseRequest
  implements Serializable
{
  private static final long serialVersionUID = -2110303371883968572L;
  private String account;
  private String password;

  public String getAccount()
  {
    return this.account;
  }

  public void setAccount(String account) {
    this.account = account;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}